from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render, redirect

from . import forms
from . import models


def components_form_action(request):
    """

    :param request:
    :return: form.ComponentForm, Bool
    """
    # if this is a POST request we need to process the form data
    if request.method == 'POST' and "submit_component" in request.POST:
        # create a form instance and populate it with data from the request:
        form = forms.ComponentForm(data=request.POST, origin_url=request.get_full_path())
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            recipe = form.save(commit=False)
            recipe.author = request.user
            recipe.save()

            # redirect to the edit page
            return form, True


    # if a GET (or any other method) we'll create a blank form
    else:
        form = forms.ComponentForm(origin_url=request.get_full_path())

    return form, False


def ingredients_form_action(request, recipe):
    # if this is a POST request we need to process the form data
    # ReciFormSet = modelformset_factory(Recipe, form=RecipeForm)
    if request.method == 'POST' and "submit_ingredient" in request.POST:
        # create a form instance and populate it with data from the request:
        form = forms.IngredientForm(data=request.POST, origin_url=request.get_full_path())
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            ingredient = form.save(commit=False)
            ingredient.author = request.user
            ingredient.recipe = recipe
            ingredient.save()

            return form, True

    # if a GET (or any other method) we'll create a blank form
    else:
        form = forms.IngredientForm(origin_url=request.get_full_path())

    return form, False


@login_required
def index(request):
    recipes = models.Recipe.objects.filter(is_public=True).order_by('-pub_date')[:5]
    context = {'recipes_list': recipes}
    return render(request, 'index.html', context)


"""
    Recipes views
"""


@login_required
def recipes_list(request):
    recipes = models.Recipe.objects.filter(is_public=True).order_by('-pub_date')
    context = {'recipes_list': recipes}
    return render(request, 'recipes/list_public.html', context)


@login_required
def recipes_list_own(request):
    recipes = models.Recipe.objects.filter(author=request.user).order_by('-pub_date')
    context = {'recipes_list': recipes}
    return render(request, 'recipes/list.html', context)


@login_required
def recipes_add(request):
    # if this is a POST request we need to process the form data
    # ReciFormSet = modelformset_factory(Recipe, form=RecipeForm)
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        print(request.user)
        form = forms.RecipeForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            recipe = form.save(commit=False)
            recipe.author = request.user
            recipe.save()

            # redirect to the edit page
            return redirect('alchemy:recipes_edit', recipe_id=recipe.id)

    # if a GET (or any other method) we'll create a blank form
    else:
        form = forms.RecipeForm()
    return render(request, 'recipes/add.html', {'form': form})


@login_required
def recipes_edit_info(request, recipe_id):
    recipe = get_object_or_404(models.Recipe, pk=recipe_id)
    # if this is a POST request we need to process the form data
    # ReciFormSet = modelformset_factory(Recipe, form=RecipeForm)
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        print(request.user)
        form = forms.RecipeForm(request.POST, instance=recipe)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            recipe = form.save(commit=False)
            recipe.author = request.user
            recipe.save()

            # redirect to the edit page
            return redirect('alchemy:recipes_edit', recipe_id=recipe.id)

    # if a GET (or any other method) we'll create a blank form
    else:
        form = forms.RecipeForm(instance=recipe)
    return render(request, 'recipes/edit_info.html', {'form': form})


@login_required
def recipes_view(request, recipe_id):
    recipe = get_object_or_404(models.Recipe, pk=recipe_id)
    ingredients = models.Ingredient.objects.filter(recipe=recipe)
    return render(request, 'recipes/watch.html', {'recipe': recipe,
                                                  "ingredients": ingredients})


@login_required
def recipes_edit(request, recipe_id):
    recipe = get_object_or_404(models.Recipe, pk=recipe_id)

    components_form, res1 = components_form_action(request)
    ingredient_form, res2 = ingredients_form_action(request, recipe)

    if res1 or res2:
        if request.GET.get('form_origin'):
            url_redirect = request.GET.get('form_origin')
            return redirect(url_redirect)
        else:
            url_redirect = request.GET.get('next') if request.GET.get(
                'next') is not None else 'alchemy:recipes_edit'

            return redirect(url_redirect, recipe_id=recipe.id)

    ingredients = models.Ingredient.objects.filter(recipe=recipe)
    return render(request, 'recipes/edit.html',
                  {'recipe': recipe,
                   'component_form': components_form,
                   "ingredients_list": ingredients,
                   'ingredient_form': ingredient_form
                   })


@login_required
def recipes_remove(request, recipe_id):
    recipe = get_object_or_404(models.Recipe, pk=recipe_id)

    recipe.delete()

    return redirect('alchemy:recipes_list')


@login_required
def recipes_vote(request, recipe_id):
    recipe = get_object_or_404(models.Recipe, pk=recipe_id)
    # TODO:


"""
    Recipes kinds views
"""


def recipes_kinds_index(request):
    brands = models.RecipeKind.objects.order_by('-name')[:5]
    context = {'kinds': brands}
    return render(request, 'recipes/kinds_list.html', context)


@login_required
def recipes_kinds_add(request):
    # if this is a POST request we need to process the form data
    # ReciFormSet = modelformset_factory(Recipe, form=RecipeForm)
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        print(request.user)
        form = forms.RecipeKindForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            recipe_kind = form.save(commit=False)
            recipe_kind.author = request.user
            recipe_kind.save()

            # redirect to the edit page
            return redirect('alchemy:recipes_kinds_list')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = forms.RecipeKindForm()
    return render(request, 'recipes/kinds_add.html', {'form': form})


@login_required
def recipes_kinds_edit(request, brand_id):
    brand = get_object_or_404(models.RecipeKind, pk=brand_id)


"""
    Components views
"""


@login_required
def components_index(request):
    components = models.Component.objects.order_by('-name')

    components_form, res1 = components_form_action(request)

    if res1:
        return redirect(request.get_full_path())

    context = {'components_list': components,
               'component_form': components_form}

    return render(request, 'components/index.html', context)


@login_required
def components_edit(request, component_id):
    component = get_object_or_404(models.Component, pk=component_id)

    # if this is a POST request we need to process the form data
    # ReciFormSet = modelformset_factory(Recipe, form=RecipeForm)
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        print(request.user)
        form = forms.ComponentForm(data=request.POST, instance=component, origin_url=request.get_full_path())
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # component = form.save(commit=False)
            # component.author = request.user
            component.save()

            # redirect to the edit page
            return redirect('alchemy:components_list')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = forms.ComponentForm(instance=component, origin_url=request.get_full_path())

    return render(request, 'components/edit.html',
                  {
                      'component': component,
                      'component_form': form
                  })


@login_required
def components_remove(request, component_id):
    recipe = get_object_or_404(models.Component, pk=component_id)

    recipe.delete()

    return redirect('alchemy:components_list')


"""
    Recipes kinds views
"""


def components_kinds_index(request):
    brands = models.RecipeKind.objects.order_by('-name')[:5]
    context = {'kinds': brands}
    return render(request, 'components/kinds_list.html', context)


@login_required
def components_kinds_add(request):
    # if this is a POST request we need to process the form data
    # ReciFormSet = modelformset_factory(Recipe, form=RecipeForm)
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        print(request.user)
        form = forms.ComponentKindForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            recipe_kind = form.save(commit=False)
            recipe_kind.author = request.user
            recipe_kind.save()

            if request.GET.get('form_origin'):
                url_redirect = request.GET.get('form_origin')
            else:
                url_redirect = request.GET.get('next') if request.GET.get(
                    'next') is not None else 'alchemy:components_kinds_list'

            return redirect(url_redirect)

    # if a GET (or any other method) we'll create a blank form
    else:
        form = forms.ComponentKindForm()
    return render(request, 'components/kinds_add.html', {'form': form})


@login_required
def components_kinds_edit(request, brand_id):
    brand = get_object_or_404(models.RecipeKind, pk=brand_id)


"""
    Brands views
"""


def brands_index(request):
    brands = models.Brand.objects.order_by('-name')[:5]
    context = {'brands_list': brands}
    return render(request, 'brands/index.html', context)


@login_required
def brands_add(request):
    # if this is a POST request we need to process the form data
    # ReciFormSet = modelformset_factory(Recipe, form=RecipeForm)
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        print(request.user)
        form = forms.BrandForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            recipe = form.save(commit=False)
            recipe.author = request.user
            recipe.save()

            if request.GET.get('form_origin'):
                url_redirect = request.GET.get('form_origin')
            else:
                url_redirect = request.GET.get('next') if request.GET.get(
                    'next') is not None else 'alchemy:components_kinds_list'

            return redirect(url_redirect)

    # if a GET (or any other method) we'll create a blank form
    else:
        form = forms.BrandForm()
    return render(request, 'brands/add.html', {'form': form})


@login_required
def brands_edit(request, brand_id):
    brand = get_object_or_404(models.Brand, pk=brand_id)


@login_required
def brands_remove(request, brand_id):
    recipe = get_object_or_404(models.Brand, pk=brand_id)

    recipe.delete()

    return redirect('alchemy:brands_list')


"""
    REST API
"""

from . import serializers
from rest_framework import viewsets
from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner of the snippet.
        return obj.author == request.user


class RecipesViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = models.Recipe.objects.all()
    serializer_class = serializers.RecipeSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class IngredientsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = models.Ingredient.objects.all()
    serializer_class = serializers.IngredientSerializer


class ComponentsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = models.Component.objects.all().order_by('name')
    serializer_class = serializers.ComponentSerializer
