from django.apps import AppConfig


class AlchemyConfig(AppConfig):
    name = 'alchemy'
