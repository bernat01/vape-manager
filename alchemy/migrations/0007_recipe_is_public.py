# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2019-08-03 14:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('alchemy', '0006_auto_20190714_1140'),
    ]

    operations = [
        migrations.AddField(
            model_name='recipe',
            name='is_public',
            field=models.BooleanField(default=False),
        ),
    ]
