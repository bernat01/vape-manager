from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericRelation
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.urls import reverse
from django.utils.safestring import mark_safe
from star_ratings.models import Rating


class RecipeKind(models.Model):
    name = models.CharField(max_length=200)
    description = models.fields.TextField(default='')
    author = models.ForeignKey(User, on_delete=models.PROTECT)

    def __str__(self):
        return self.name + " - " + self.description


class Recipe(models.Model):
    title = models.CharField(max_length=200)
    description = models.fields.TextField(default='')
    pub_date = models.DateTimeField('date published', auto_now_add=True)
    kind = models.ForeignKey(RecipeKind, on_delete=models.PROTECT, default=None, null=True)
    author = models.ForeignKey(User, on_delete=models.PROTECT)
    ratings = GenericRelation(Rating, related_query_name='recipe_ratings')
    is_public = models.BooleanField(default=False)

    def is_public_icon(self):
        if self.is_public:
            output = '<i class="fas fa-check" style="font-size: 2em; color: Green;"></i>'
        else:
            output = '<i class="fas fa-times" style="font-size: 2em; color: Red;"></i>'

        return mark_safe(output)

    def get_ingredients(self):
        return Ingredient.objects.filter(recipe=self)

    def get_absolute_url(self):
        return reverse('alchemy:recipes_edit', kwargs={"recipe_id": self.id})

    def get_view_url(self):
        return reverse('alchemy:recipes_view', kwargs={"recipe_id": self.id})

    def get_average_ratings(self):
        if self.ratings.count() > 0:
            return self.ratings.get().average
        else:
            return 0.0

    def __str__(self):
        return self.title


class Brand(models.Model):
    name = models.CharField(max_length=200)
    url = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class ComponentKind(models.Model):
    name = models.CharField(max_length=200)
    description = models.fields.TextField()

    def __str__(self):
        return self.name


class Component(models.Model):
    name = models.CharField(max_length=200)
    description = models.fields.TextField()
    author = models.ForeignKey(User, on_delete=models.PROTECT)
    brand = models.ForeignKey(Brand, on_delete=models.PROTECT, default=None, null=True)
    kind = models.ForeignKey(ComponentKind, on_delete=models.PROTECT, default=None, null=True)

    def __str__(self):
        return self.name + " - " + str(self.brand)


class Ingredient(models.Model):
    recipe = models.ForeignKey(Recipe, related_name='ingredients', on_delete=models.CASCADE)
    component = models.ForeignKey(Component, on_delete=models.PROTECT)
    percentage = models.FloatField()

    def __str__(self):
        return str(self.component) + " - " + str(self.percentage) + "%"


class RecipeValoration(models.Model):
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    value = models.PositiveIntegerField(default=10, validators=[MinValueValidator(0), MaxValueValidator(10)])
    comment = models.fields.TextField(default='')
