function Recipe(recipe_id) {
    this.loading = false;
    this.object = null;
    this.ingredients = [];
    this.messages = {
        error: []
    };
    this.get = function () {
        this.loading = true;
        axios.get('alchemy/api/recipes/' + recipe_id + '/')
            .then((response) => {
                this.object = response.data;
                console.log(this.object);
                this.ingredients = this.object.ingredients;
                this.loading = false;
            })
            .catch((err) => {
                this.loading = false;
                console.log(err);
            })
    };

    this.addIngredient = function (ingredient) {
        this.loading = true;
        axios.post('alchemy/api/ingredients/', ingredient)
            .then((response) => {
                this.loading = false;
                this.get();
            })
            .catch((err) => {
                this.loading = false;
                console.log(err);
                console.log(err.response);
                console.log(err.response.data);
                this.messages.error = err.response.data.non_field_errors;
            })
    };

    this.updateIngredient = function (ingredient_id, data) {
        this.loading = true;
        axios.put('alchemy/api/ingredients/' + ingredient_id + '/', data)
            .then((response) => {
                this.loading = false;
                this.get();
                $('#modal-edit-ingredient').modal('hide')
            })
            .catch((err) => {
                this.loading = false;
                console.log(err);
                console.log(err.response);
                console.log(err.response.data);
            })
    };

    this.removeIngredient = function (ingredient_id) {
        this.loading = true;
        axios.delete('alchemy/api/ingredients/' + ingredient_id + '/')
            .then((response) => {
                this.loading = false;
                this.get();
            })
            .catch((err) => {
                this.loading = false;
                console.log(err);
                console.log(err.response);
                console.log(err.response.data);
            })
    }
}