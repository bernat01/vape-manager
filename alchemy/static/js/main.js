function remove_event($item) {
    var url = $item.attr("data-link");
    if (confirm("Are you sure you want to delete this record?")) {
        window.location.href = url;
    }
}

function initComplete(settings, json) {
    // TODO: place here table events - concretly delete row
    $(".btn-delete-row").on("click", function () {
        remove_event($(this));
    });
}

function buttonAddDef($table, text) {
    return {
        text: text,
        className: 'btn btn-success',
        action: function (e, dt, node, config) {
            window.location.href = $table.attr("data-link-add");
        }
    }
}

$(document).ready(function () {

    var $recipes_table = $('#table-recipes');
    var recipes_table = $recipes_table.DataTable({
        "columnDefs": [
            {
                "targets": -3,
                "width": "5%"
            },
            {
                "targets": -2,
                "width": "5%"
            },
            {
                "targets": -1,
                "width": "5%"
            }
        ],
        "initComplete": initComplete,
        lengthChange: false,
        buttons: [
            buttonAddDef($recipes_table, "Add Recipe")
        ]

    });

    recipes_table.buttons().container()
        .appendTo('#table-recipes_wrapper .col-md-6:eq(0)');

    var $recipes_public_table = $('#table-recipes-public');
    var recipes_public_table = $recipes_public_table.DataTable({
        "columnDefs": [
            {
                "targets": -1,
                "width": "5%"
            }
        ],
        "initComplete": initComplete,
        lengthChange: false,
        buttons: [
            buttonAddDef($recipes_public_table, "Add Recipe")
        ]

    });

    recipes_public_table.buttons().container()
        .appendTo('#table-recipes-public_wrapper .col-md-6:eq(0)');

    var $components_table = $('#table-components');
    var components_table = $components_table.DataTable({
        "columnDefs": [
            {
                "targets": -2,
                "width": "5%"
            },
            {
                "targets": -1,
                "width": "5%"
            }
        ],
        "initComplete": initComplete,
        lengthChange: false,
        buttons: [
            {
                text: "Add Component",
                className: 'btn btn-success',
                action: function (e, dt, node, config) {
                    // window.location.href = $components_table.attr("data-link-add");
                    //TODO: open modal with javascript -> "#"
                    $("#modal-form-component").modal();
                }
            }
        ]

    });

    components_table.buttons().container()
        .appendTo('#table-components_wrapper .col-md-6:eq(0)');

    var $brands_table = $('#table-brands');
    var brands_table = $brands_table.DataTable({
        "columnDefs": [
            {
                "targets": -2,
                "width": "5%"
            },
            {
                "targets": -1,
                "width": "5%"
            }
        ],
        "initComplete": initComplete,
        lengthChange: false,
        buttons: [
            buttonAddDef($brands_table, "Add Brand")
        ]

    });

    brands_table.buttons().container()
        .appendTo('#table-brands_wrapper .col-md-6:eq(0)');
});