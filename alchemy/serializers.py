from rest_framework import serializers

from .models import Recipe, Ingredient, Component


class ComponentSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="alchemy:component-detail")
    brand = serializers.StringRelatedField()

    class Meta:
        model = Component
        fields = ['url', 'id', 'name', 'description', 'brand']


class IngredientSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="alchemy:ingredient-detail")
    component = ComponentSerializer(required=False)

    recipe_id = serializers.IntegerField(write_only=True)
    component_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Ingredient
        fields = ['url', 'id', 'component', 'percentage', 'recipe_id', 'component_id']

    def validate(self, attrs):
        attrs = super(IngredientSerializer, self).validate(attrs)  # calling default validation

        sum_porcs = attrs['percentage']
        for ingredient in Recipe.objects.get(id=attrs['recipe_id']).get_ingredients():
            sum_porcs += ingredient.percentage
        if sum_porcs > 100:
            raise serializers.ValidationError("Ingredients percentage can't be greater than 100.")
        return attrs


class RecipeSerializer(serializers.HyperlinkedModelSerializer):
    ingredients = IngredientSerializer(many=True, read_only=True)

    kind = serializers.StringRelatedField(read_only=True)
    url = serializers.HyperlinkedIdentityField(view_name="alchemy:recipe-detail")
    view_url = serializers.CharField(source='get_view_url')
    average_ratings = serializers.FloatField(source='get_average_ratings')

    class Meta:
        model = Recipe
        fields = ['url', 'id', 'title', 'description', 'pub_date', 'kind', 'is_public', 'ingredients', 'view_url',
                  'average_ratings']

    def validate(self, attrs):
        attrs = super(RecipeSerializer, self).validate(attrs)  # calling default validation

        if attrs['is_public'] == True:
            sum_porcs = 0.0
            for ingredient in Recipe.objects.get(id=attrs['id']).get_ingredients():
                sum_porcs += ingredient.percentage

            if sum_porcs != 100:
                raise serializers.ValidationError("The recibe ingredients should sum 100 to make it public.")
        return attrs

    def validate_ingredients(self):
        ...
