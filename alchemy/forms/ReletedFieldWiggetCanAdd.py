from django.core.urlresolvers import reverse
from django.forms import widgets
from django.utils.safestring import mark_safe


class RelatedFieldWidgetCanAdd(widgets.Select):

    def __init__(self, related_model, related_url, origin_url=None, modal_selector=None, *args, **kw):

        super(RelatedFieldWidgetCanAdd, self).__init__(*args, **kw)

        # if not related_url:
        #     rel_to = related_model
        #     info = (rel_to._meta.app_label, rel_to._meta.object_name.lower())
        #     related_url = 'alchemy:%s_%s_add' % info

        # Be careful that here "reverse" is not allowed
        self.first_render = True
        self.related_url = related_url
        self.origin_url = origin_url

        self.modal_selector = modal_selector

    def render(self, name, value, *args, **kwargs):
        if self.first_render:
            self.first_render = False
            self.related_url = reverse(self.related_url)
            self.related_url += "?form_origin=" + self.origin_url if self.origin_url else ""

        output = ["<div class='input-group'>"]
        output += [super(RelatedFieldWidgetCanAdd, self).render(name, value, *args, **kwargs)]
        if self.modal_selector is None:
            output += [(u'<a href="%s" class="btn btn-success" id="add_id_%s"><i class="fas fa-plus"></i></a></div>' %
                        (self.related_url, name))]
        else:
            output += [(
                    u'<button href="%s" class="btn btn-success" id="add_id_%s" data-toggle="modal" data-target="%s"><i class="fas fa-plus"></i></button></div>' %
                    (self.related_url, name, self.modal_selector))]

        return mark_safe(u''.join(output))
