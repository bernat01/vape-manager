from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from django.core.exceptions import NON_FIELD_ERRORS
from django.utils.translation import gettext_lazy as _

from alchemy.models import Brand


class BrandForm(forms.ModelForm):
    class Meta:
        model = Brand
        # exclude = ["author"]
        fields = ('name', 'url')
        labels = {
            'title': _('Nombre'),
        }
        help_texts = {
            'title': _('Especifica el nombre de la marca.'),
        }
        error_messages = {
            'title': {
                'max_length': _("El nombre de la marca es demasiado largo."),
            },

            # Overwrite default messages
            NON_FIELD_ERRORS: {
                'unique_together': "%(model_name)s's %(field_labels)s are not unique.",
            }
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Save brand'))

    def clean_name(self):
        # custom validation for the name field
        return self.cleaned_data['name']
