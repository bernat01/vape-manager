from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from django.core.exceptions import NON_FIELD_ERRORS
from django.utils.translation import gettext_lazy as _

from alchemy.models import Ingredient, Component
from .ReletedFieldWiggetCanAdd import RelatedFieldWidgetCanAdd


class IngredientForm(forms.ModelForm):
    class Meta:
        model = Ingredient
        exclude = ["recipe"]
        fields = ('component', 'percentage')
        labels = {
            'component': _('Component'),
        }
        help_texts = {
            'component': _('Ingredient name.'),
            'percentage': _('Portion of the component in the recipe.'),
        }
        error_messages = {
            'percentage': {
                'max_value': _("Too high percentage."),
            },

            # Overwrite default messages
            NON_FIELD_ERRORS: {
                'unique_together': "%(model_name)s's %(field_labels)s are not unique.",
            }
        }

        widgets = {
            'component': RelatedFieldWidgetCanAdd(Component, origin_url=None, related_url='alchemy:components_list',
                                                  attrs={'class': 'form-control'})
        }

    def __init__(self, origin_url=None, modal_component_selector="#modal-form-component", *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit_ingredient', 'Save ingredient'))

        self.fields["component"].widget.modal_selector = modal_component_selector

    def clean_percentage(self):
        # custom validation for the name field
        # TODO Comprovar que no suma més de 100 amb el dels altres ingredients
        return self.cleaned_data['percentage']
