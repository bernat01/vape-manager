from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from django.core.exceptions import NON_FIELD_ERRORS
from django.utils.translation import gettext_lazy as _

from .ReletedFieldWiggetCanAdd import RelatedFieldWidgetCanAdd
from ..models import Recipe, RecipeKind


class RecipeForm(forms.ModelForm):
    class Meta:
        model = Recipe
        # exclude = ["author"]
        fields = ('title', 'kind', 'description', "is_public")
        labels = {
            'title': _('Name'),
            'is_public': _('Make it public')
        }
        help_texts = {
            'title': _('Dale un nombre a la receta.'),
        }
        error_messages = {
            'title': {
                'max_length': _("El nombre de la receta es demasiado largo."),
            },

            # Overwrite default messages
            NON_FIELD_ERRORS: {
                'unique_together': "%(model_name)s's %(field_labels)s are not unique.",
            }
        }

        widgets = {
            'description': forms.Textarea(attrs={'cols': 20, 'rows': 5}),
            'kind': RelatedFieldWidgetCanAdd(RecipeKind, related_url="alchemy:recipes_kinds_add",
                                             attrs={'class': 'form-control'})
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit_recipe', 'Save recipe'))

    def clean_name(self):
        # custom validation for the name field
        ...
