from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from django.core.exceptions import NON_FIELD_ERRORS
from django.utils.translation import gettext_lazy as _

from alchemy.models import Component, Brand, ComponentKind
from .ReletedFieldWiggetCanAdd import RelatedFieldWidgetCanAdd


class ComponentForm(forms.ModelForm):
    class Meta:
        model = Component
        fields = ('name', 'description', "brand", "kind")
        labels = {
            'name': _('Name'),
        }
        help_texts = {
            'name': _('Name of the component.'),
        }
        error_messages = {
            # Overwrite default messages
            NON_FIELD_ERRORS: {
                'unique_together': "%(model_name)s's %(field_labels)s are not unique."
            }
        }

        widgets = {
            'description': forms.Textarea(attrs={'cols': 20, 'rows': 5}),
            'brand': RelatedFieldWidgetCanAdd(Brand, related_url="alchemy:brands_add", origin_url=None,
                                              attrs={'class': 'form-control'}),
            'kind': RelatedFieldWidgetCanAdd(ComponentKind, related_url="alchemy:components_kinds_add", origin_url=None,
                                             attrs={'class': 'form-control'})

        }

    def __init__(self, origin_url=None, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["brand"].widget.origin_url = origin_url
        self.fields["kind"].widget.origin_url = origin_url

        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit_component', 'Register component'))
