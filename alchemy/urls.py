from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from rest_framework.routers import DefaultRouter

from . import views

app_name = 'alchemy'

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'recipes', views.RecipesViewSet)
router.register(r'ingredients', views.IngredientsViewSet)
router.register(r'components', views.ComponentsViewSet)

urlpatterns = [
    url(r'^$', views.index, name='index'),

    # The API URLs are now determined automatically by the router.
    url('api/', include(router.urls)),

]

"""
    RECIPES
"""
urlpatterns += [
    url(r'^recipes/$', views.recipes_list, name='recipes_list'),

    url(r'^recipes/own/$', views.recipes_list_own, name='recipes_list_own'),

    url(r'^recipes/add/$', views.recipes_add, name='recipes_add'),

    url(r'^recipes/(?P<recipe_id>[0-9]+)/edit/info/$', views.recipes_edit_info, name='recipes_edit_info'),

    # Main recipe page
    url(r'^recipes/(?P<recipe_id>[0-9]+)/$', views.recipes_view, name='recipes_view'),

    # Edit recipe page
    url(r'^recipes/(?P<recipe_id>[0-9]+)/edit/$', views.recipes_edit, name='recipes_edit'),

    # Remove a recipe
    url(r'^recipes/(?P<recipe_id>[0-9]+)/remove/$', views.recipes_remove, name='recipes_remove'),

    # Vote recipe page
    url(r'^recipes/(?P<recipe_id>[0-9]+)/vote/$', views.recipes_vote, name='recipes_vote'),

]

"""
    RECIPES KINDS
"""
urlpatterns += [
    url(r'^recipes/kinds/$', views.recipes_kinds_index, name='recipes_kinds_list'),

    # Register new brand in database
    url(r'^recipes/kinds/add/$', views.recipes_kinds_add, name='recipes_kinds_add'),

    url(r'^recipes/kinds/edit/(?P<brand_id>[0-9]+)$', views.recipes_kinds_edit, name='recipes_kinds_edit')

]

"""
    COMPONENTS
"""
urlpatterns += [

    # View all components
    url(r'^components/$', views.components_index, name='components_list'),

    # Edit a component
    url(r'^components/(?P<component_id>[0-9]+)/edit/$', views.components_edit, name='components_edit'),

    # Remove a component
    url(r'^components/(?P<component_id>[0-9]+)/remove/$', views.components_remove, name='components_remove'),

]

"""
    COMPONENTS KINDS
"""
urlpatterns += [
    url(r'^components/kinds/$', views.components_kinds_index, name='components_kinds_list'),

    # Register new brand in database
    url(r'^components/kinds/add/$', views.components_kinds_add, name='components_kinds_add'),

    url(r'^components/kinds/edit/(?P<brand_id>[0-9]+)$', views.components_kinds_edit, name='components_kinds_edit')

]

"""
    BRANDS
"""
urlpatterns += [
    url(r'^brands/$', views.brands_index, name='brands_list'),

    # Register new brand in database
    url(r'^brands/add/$', views.brands_add, name='brands_add'),

    url(r'^brands/edit/(?P<brand_id>[0-9]+)$', views.brands_edit, name='brands_edit'),

    url(r'^brands/remove/(?P<brand_id>[0-9]+)$', views.brands_remove, name='brands_remove')

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
