#!/usr/bin/env bash

mysql -u root -proot -e "CREATE DATABASE vape_manager";

mysql -u root -proot -e "CREATE USER 'vapeManager'@'localhost' IDENTIFIED BY '123456';";

mysql -u root -proot -e "GRANT ALL PRIVILEGES ON vape_manager.* TO 'vapeManager'@'localhost';";

mysql -u root -proot -e "GRANT FILE ON *.* TO 'vapeManager'@'localhost';";

#mysql -u vapeManager -p 123456 -D 'vape_manager' -e "SQL_QUERY"