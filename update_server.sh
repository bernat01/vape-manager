#!/usr/bin/env bash

git fetch --all

git reset --hard origin/master

python manage.py collectstatic

# SET TO TRUE --> PRODUCTION setting
echo "SET TO TRUE --> PRODUCTION setting"

# Restart service
service apache2 restart