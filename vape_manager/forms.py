from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Div, ButtonHolder
from django import forms


class ContactForm(forms.Form):
    name = forms.CharField(max_length=100)
    email = forms.EmailField()
    message = forms.CharField(widget=forms.Textarea)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.layout = Layout(
            Div(
                Div('name', css_class="col-md-6"),
                Div('email', css_class="col-md-6"),
                css_class="row"
            ),
            Div(
                Div('message', css_class="col-md-12"),
                css_class="row"
            ),
            ButtonHolder(
                Submit('submit', 'Send Message')
            ),
        )
