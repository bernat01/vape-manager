#!/usr/bin/env bash

source activate vape-manager

python manage.py makemigrations alchemy
python manage.py makemigrations user_registration_bs4
python manage.py makemigrations user_profile

python manage.py sqlmigrate alchemy 0001
python manage.py sqlmigrate user_registration_bs4 0001
python manage.py sqlmigrate user_profile 0001

python manage.py migrate
