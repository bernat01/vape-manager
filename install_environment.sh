#!/usr/bin/env bash

##
## Commands used to install the environment
##
apt install virtualenv

virtualenv --python=python3 env

source env/bin/activate

pip install -r requirements.txt
